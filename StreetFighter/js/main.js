import { Ryu } from "./ryu.js";
import { Ken } from "./ken.js";
import { Stage } from "./stage.js";


const GameViewPort = {
    WIDTH: 450,
    HEIGHT: 250,
};

window.onload = function () {

    const canvas = document.querySelector('canvas');
    const context = canvas.getContext('2d');

    canvas.width = GameViewPort.WIDTH;
    canvas.height = GameViewPort.HEIGHT;

    const ryu = new Ryu(80,120,1);
    const ken = new Ken(80,120,-1);
    const stage = new Stage();

    function frame() {
        ryu.update(context);
        ken.update(context);

        stage.draw(context);

        ryu.draw(context);
        ken.draw(context);
        window.requestAnimationFrame(frame);
    }

    window.requestAnimationFrame(frame);
}
